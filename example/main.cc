/**
 * File main.cc
 * Author: Jeffrey Martin - jeffrey.martin04@gmail.com
 *
 * This file demonstrates how to use the i3treeparser library. 
 * Compilation instructions are found in the Makefile.
 *
 * This program simple creates a Tree instance and prints out the active 
 * Display/Workspace/Window
 */

#include <iostream>
#include <vector>

#include <i3treeparser/i3treeparser.h>

int main(int argc, char* argv[]){
    // Create an i3 Tree instance using the TreeBuilder
    i3treeparser::Tree* tree = i3treeparser::TreeBuilder::build();
    
    // Iterate over all the displays 
    std::vector<i3treeparser::Display*> displays = tree->getDisplays();
    for(int i = 0; i < displays.size(); i++){   
        // Get current display
        i3treeparser::Display* display = displays[i];
        if(display->isActive()){
            std::cout << "Display: " << display->getName() << std::endl;
            // Iterate over all workspaces
            std::vector<i3treeparser::Workspace*> workspaces = display->getWorkspaces(); 
            for(int j = 0; j < workspaces.size(); j++){
                // Get current workspace
                i3treeparser::Workspace* workspace = workspaces[j];
                if(workspace->isFocused()){
                    std::cout << "Workspace: " << workspace->getName() << std::endl; 
                    // Traverse the layout tree (using a stack to avoid recursion)
                    std::vector<i3treeparser::Layout*> layout_stack; 
                    if(workspace->getLayout() != NULL){
                        layout_stack.push_back(workspace->getLayout());
                    }
                    while(!layout_stack.empty()){
                        // Get current layout
                        i3treeparser::Layout* layout = layout_stack.back();
                        layout_stack.pop_back();
                        // Get Layout's window (if it has one)
                        i3treeparser::Window* window = layout->getChildWindow();
                        // Get Layout's sub layouts (if it has them)
                        std::vector<i3treeparser::Layout*> sub_layouts = layout->getSubLayouts();
                        if(window != NULL){
                            if(window->isFocused()){
                                std::cout << "Window: " << window->getType() << std::endl;
                            }
                        }else{
                            layout_stack.insert(layout_stack.end(), sub_layouts.begin(), sub_layouts.end());
                        }
                    }
                }
            }
        }

    }

    // Free the i3 Tree instance using the TreeBuider
    i3treeparser::TreeBuilder::deleteTree(tree);
};
