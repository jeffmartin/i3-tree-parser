/**
 * File: Tree.cc
 * Author Jeffrey Martin - jeffrey.martin04@gmail.com
 */

#include <i3treeparser/Tree.h>

#include <iostream>

#include <json/json.h>

#include <i3treeparser/Display.h>

namespace i3treeparser{

Tree::Tree(Json::Value json_tree, Json::Value json_displays, 
        Json::Value json_workspaces){
    // Iterate over each display in the tree
    for(int i = 0; i < json_tree["nodes"].size(); i++){
        Json::Value json_tree_display = json_tree["nodes"][i];
        // Skip the "__i3" virtual display
        std::string display_name = json_tree_display["name"].asString();
        if(!display_name.compare("__i3")) continue;
        // Find the corrusponding display object in json_displays;
        Json::Value json_display;
        for(int j = 0; j < json_displays.size(); j++){
            if(!display_name.compare(json_displays[j]["name"].asString())){
                json_display = json_displays[j];
                break;
            }
        }
        // Create a new display object and add it to the list of displays
        displays.push_back(new Display(json_tree_display, json_display, 
                    json_workspaces));
    }
}

}; // namespace i3treeparser

