/** 
 * File: Tree.h
 * Author: Jeffrey Martin - jeffrey.martin04@gmail.com
 */

#ifndef I3TREEPARSER_TREE_H
#define I3TREEPARSER_TREE_H

#include <vector>

#include <json/json.h>

#include <i3treeparser/Display.h>

namespace i3treeparser{

/**
 * This class represents the overarching tree structure of i3 window manager.
 */
class Tree{

private:
    /** The Displays belonging to this i3 Tree. */
    std::vector<i3treeparser::Display*> displays;

public:
    /** 
     * @param json_tree The Json object representing this Tree. Retrieved from
     * the TreeBuilder's tree_query.
     * @param json_displays The json object representing the displays.
     * Retrieved from the TreeBuilder's displays_query.
     * @param json_workspaces The json object representing the workspaces.
     * Retrieved from the TreeBuilder's workspaces_query.
     * */
    Tree(Json::Value json_tree, Json::Value json_displays, 
        Json::Value json_workspaces);
    /** @return A constant reference to the Tree's Displays. */
    const std::vector<i3treeparser::Display*>& getDisplays(){ return displays; };
    /** @return The number of Displays in the Tree. */
    int getNumDisplays(){ return displays.size(); }

}; // class Tree

}; // namespace i3treeparser

#endif // I3TREEPARSER_TREE_H
