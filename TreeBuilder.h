/**
 * File: TreeBuilder.h 
 * Author: Jeffrey Martin - jeffrey.martin04@gmail.com
 */

#ifndef I3TREEPARSER_TREEBUILDER_H
#define I3TREEPARSER_TREEBUILDER_H

#include <string>
#include <vector>

#include <json/json.h>

#include <i3treeparser/Tree.h>

namespace i3treeparser{

/**
 * This class provides interface to i3 that is needed to construct a Tree.
 */
class TreeBuilder{

private:
    /** Private Constructor to make the class static. */
    TreeBuilder();
    /*
     * @param command The shell command to run.
     * @return A pointer to a string that holds the contents of stdout after
     * the execution of the given command.
     */
    static std::string executeForResult(const std::string command);
    /*
     * @param json_string The JSON object as a string.
     * @param val The Json value to be populated.
     * @return True if the json_string was successfully parsed, false 
     * otherwise.
     */
    static bool buildDOM(std::string json_string, Json::Value& val);
    /** i3 State queries. */
    static const std::string tree_query;
    static const std::string displays_query;
    static const std::string workspaces_query;
public:
    /** This function constructs a new instance of the i3 Tree. */
    static i3treeparser::Tree* build();
    /** This function deletes an instance of the i3 Tree */
    static void deleteTree(i3treeparser::Tree* tree);

}; // class TreeBuilder

}; // namespace i3treeparser

#endif // I3TREEPARSER_TREE_H
