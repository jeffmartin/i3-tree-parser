/**
 * File: Workspace.cc
 * Author Jeffrey Martin - jeffrey.martin04@gmail.com
 */

#include <i3treeparser/Workspace.h>

#include <iostream>
#include <string>
#include <vector>

#include <json/json.h>

#include <i3treeparser/Layout.h>
#include <i3treeparser/Window.h>

namespace i3treeparser{

Workspace::Workspace(Json::Value json_tree_workspace, Json::Value json_workspace){
    visible = json_workspace["visible"].asBool();
    focused = json_workspace["focused"].asBool();
    urgent = json_tree_workspace["urgent"].asBool();
    number = json_tree_workspace["num"].asInt();
    name = json_tree_workspace["name"].asString();
    layout = new Layout(json_tree_workspace);
    /* Retrieve all the Windows */
    std::vector<i3treeparser::Layout*> layout_stack;
    layout_stack.push_back(layout);
    while(!layout_stack.empty()){
        i3treeparser::Layout* layout = layout_stack.back();
        layout_stack.pop_back();
        if(layout->getChildWindow() != NULL){
            windows.push_back(layout->getChildWindow());
        }else{
            layout_stack.insert(layout_stack.end(), 
                    layout->getSubLayouts().begin(), 
                    layout->getSubLayouts().end());
        }
    }
}

}; // namespace i3treeparser

