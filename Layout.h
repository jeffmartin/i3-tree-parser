/**
 * File: Layout.h
 * Author: Jeffrey Martin - jeffrey.martin04@gmail.com
 */

#ifndef I3TREEPARSER_LAYOUT_H
#define I3TREEPARSER_LAYOUT_H

#include <string>
#include <vector>

#include <json/json.h>

#include <i3treeparser/Window.h>

namespace i3treeparser{

/**
 * This class represents a single layout within the i3 window manager.
 */
class Layout{

private:
   /** 
     * The sub layouts belonging to this layout. 
     * Note, empty if 'child' is not NULL.
     */
    std::vector<i3treeparser::Layout*> sub_layouts;
    /** 
     * The Window belonging to this layout.
     * Note, NULL if 'sub_layouts' is not empty.
     */
    i3treeparser::Window* child;
    /**
     * The type of layout. Can be any of the following:
     * ['splith', 'splitv', 'tabbed', 'stacked', 'container'].
     */
    std::string type;
 
public:
    /** 
      * @param json_tree_layout The Json object representing this layout. 
      * Retrieved from the TreeBuilder's tree_query. 
      */
    Layout(Json::Value json_layout);
    /** @return A constant reference to the Layout's SubLayouts. */
    const std::vector<i3treeparser::Layout*>& getSubLayouts(){ return sub_layouts; };
    /** @return A pointer to the Layout's window. */
    i3treeparser::Window* getChildWindow(){ return child; };
    /** @return The type of this layout. */
    std::string getType(){ return type; };

}; // class Layout

}; // namespace i3treeparser

#endif // I3TREEPARSER_LAYOUT_H
