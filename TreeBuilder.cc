/**
 * File: TreeBuilder.cc
 * Author: Jeffrey Martin - jeffrey.martin04@gmail.com
 */

#include <i3treeparser/TreeBuilder.h>

#include <array>
#include <cstdio>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include <json/json.h>

#include <i3treeparser/Tree.h>

namespace i3treeparser{

const std::string TreeBuilder::tree_query = "i3-msg -t get_tree";
const std::string TreeBuilder::displays_query = "i3-msg -t get_outputs";
const std::string TreeBuilder::workspaces_query = "i3-msg -t get_workspaces";

std::string TreeBuilder::executeForResult(const std::string command){
    // Code retrieved from: https://stackoverflow.com/questions/478898/how-to-
    // execute-a-command-and-get-output-of-command-within-c-using-posix
    std::string result;
    std::array<char, 128> buffer;
    std::shared_ptr<FILE> pipe(popen(command.c_str(), "r"), pclose);
    if (!pipe){
        std::cerr << "Couldn't execute command: " << command << std::endl;
    }else{
        while (!feof(pipe.get())) {
            if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
                result += buffer.data();
        }
    }
    return result;
}

bool TreeBuilder::buildDOM(std::string json_string, Json::Value& val){
    std::istringstream stream(json_string);
    Json::CharReaderBuilder builder;
    return parseFromStream(builder, stream, &val, NULL);
}

i3treeparser::Tree* TreeBuilder::build(){
    Json::Value json_tree;
    Json::Value json_displays;
    Json::Value json_workspaces;
    if(!buildDOM(executeForResult(tree_query), json_tree) ||
            !buildDOM(executeForResult(displays_query), json_displays) ||
            !buildDOM(executeForResult(workspaces_query), json_workspaces)){
        std::cerr << "Couldn't parse i3 state into json object." << std::endl;
        return NULL;
    }else{
        return new Tree(json_tree, json_displays, json_workspaces);
    }
}

void TreeBuilder::deleteTree(i3treeparser::Tree* tree){
    std::vector<i3treeparser::Display*> displays;
    std::vector<i3treeparser::Workspace*> workspaces;
    std::vector<i3treeparser::Layout*> layouts;
    std::vector<i3treeparser::Window*> windows;

    displays = tree->getDisplays();
    for(int i = 0; i < displays.size(); i++){
        workspaces = displays[i]->getWorkspaces();
        for(int j = 0; j < workspaces.size(); j++){
            layouts.push_back(workspaces[j]->getLayout());
            while(!layouts.empty()){
                std::vector<i3treeparser::Layout*> sub_layouts;
                i3treeparser::Window* window;
                sub_layouts = layouts.back()->getSubLayouts();
                window = layouts.back()->getChildWindow();
                // Free Layout
                delete(layouts.back());
                layouts.pop_back();
                if(window != NULL){
                    // Free Window
                    delete(window);
                }else{
                    layouts.insert(layouts.end(), 
                        sub_layouts.begin(), sub_layouts.end());
                }
            }
            // Free Workspace
            delete(workspaces[j]);
        }
        // Free Display
        delete(displays[i]);
    }
    // Free Tree
    delete(tree);
}

}; // namespace i3treeparser

