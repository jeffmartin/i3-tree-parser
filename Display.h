/**
 * File: Display.h
 * Author: Jeffrey Martin -jeffrey.martin04@gmail.com
 */

#ifndef I3TREEPARSER_DISPLAY_H
#define I3TREEPARSER_DISPLAY_H

#include <vector>

#include <json/json.h>

#include <i3treeparser/Workspace.h>

namespace i3treeparser{

/**
 * This class represents a single display within the i3 window manager.
 */
class Display{

private:
    /** The Workspaces belonging to this Display. */
    std::vector<i3treeparser::Workspace*> workspaces;
    /** Is the Display flagged as focused */
    bool focused;
    /** Is the Display flagged as urgent */
    bool urgent;
    /** The Display name */
    std::string name;
    /** Is the display active */
    bool active;
    /** Is this display the primary display */
    bool primary;

public:
    /** 
     * @param json_tree_display The Json object representing this Display. 
     * Retrieved from the TreeBuilder's tree_query. 
     * @param json_display The Json object representing this Display.
     * Retrieved from the TreeBuilder's displays_query.
     * @param json_workspaces The Json object representing the workspaces.
     * Retrieved from The TreeBuilder's workspaces_query.
     */
    Display(Json::Value json_tree_display, Json::Value json_display, 
            Json::Value json_workspaces);
    /** @return A constant reference to the Display's Workspaces. */
    const std::vector<i3treeparser::Workspace*>& getWorkspaces(){ return workspaces; };
    /** @return The number of Workspaces in the Display. */
    int getNumWorkspaces(){ return workspaces.size(); };
    /** @return True if the Display is focused, False otherwise. */
    bool isFocused(){ return focused; };
    /** @return True if the Display is flagged as urgent, False otherwise. */
    bool isUrgent(){ return urgent; };
    /** @return The Display name. */
    std::string getName(){ return name; };
    /** @return True if the Display is active, False otherwise. */
    bool isActive(){ return active; };
    /** @return True if the Display is the primary display, False otherwise. */
    bool isPrimary(){ return primary; };
 
}; // class Display

}; // namespace i3treeparser

#endif // I3TREEPARSER_DISPLAY_H
