/** 
 * File: Workspace.h
 * Author: Jeffrey Martin - jeffrey.martin04@gmail.com
 */

#ifndef I3TREEPARSER_WORKSPACE_H
#define I3TREEPARSER_WORKSPACE_H

#include <string>
#include <vector>

#include <json/json.h>

#include <i3treeparser/Layout.h>
#include <i3treeparser/Window.h>

namespace i3treeparser{

/**
 * This class represents a single workspace within the i3 window manager.
 */
class Workspace{

private:
    /** The layout belonging to this Workspace */
    i3treeparser::Layout* layout;
    /** Is the workspace visible to the user */
    bool visible;
    /** Is the workspace flagged as focused */
    bool focused;
    /** Is the workspace flagged as urgent */
    bool urgent;
    /** The workspace number */
    int number;
    /** The workspace name */
    std::string name;
    /** The Windows under this Workspace */
    std::vector<i3treeparser::Window*> windows;

public:
    /** 
     * @param json_tree_workspace The Json object representing this Workspace.
     * Retrieved from the TreeBuilder's tree_query.
     * @param json_workspace The Json object representing this Workspace.
     * rRetrieved from the TreeBuilder's workspaces_query.
     */
    Workspace(Json::Value json_tree_workspace, Json::Value json_workspace);
    /** @return A pointer to the Workspace's Layout. */
    i3treeparser::Layout* getLayout(){ return layout; }; 
    /** @return True if the Workspace is visible, False otherwise. */
    bool isVisible(){ return visible; };
    /** @return True if the Workspace is focused, False otherwise. */
    bool isFocused(){ return focused; };
    /** @return True if the Workspace is flagged as urgent, False otherwise. */
    bool isUrgent(){ return urgent; };
    /** @return The workspace number. */
    int getNumber(){ return number; };
    /** @return The workspace name. */
    std::string getName(){ return name; };
    /** @return The number of Windows in this Workspace. */
    int getNumberWindows(){ return windows.size(); };
    /** @return A vector of Windows under this Workspace. */
    std::vector<i3treeparser::Window*> getWindows(){ return windows; };   
}; // class Workspace

}; // namespace i3treeparser

#endif // I3TREEPARSER_WORKSPACE_H
