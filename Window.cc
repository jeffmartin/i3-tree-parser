/**
 * File: Window.cc
 * Author Jeffrey Martin - jeffrey.martin04@gmail.com
 */

#include <i3treeparser/Window.h>

#include <iostream>

#include <json/json.h>

namespace i3treeparser{

Window::Window(Json::Value json_tree_window){
    focused = json_tree_window["focused"].asBool();
    urgent = json_tree_window["urgent"].asBool();
    height = json_tree_window["rect"]["height"].asInt();
    width = json_tree_window["rect"]["width"].asInt();
    x = json_tree_window["rect"]["x"].asInt();
    y = json_tree_window["rect"]["y"].asInt();
    type = json_tree_window["window_properties"]["class"].asString();
    title = json_tree_window["window_properties"]["title"].asString();
}

}; // namespace i3treeparser

