# I3 Tree Parser

This Library is designed to simplify the process of retrieving information
from the I3 tree. 

### Installation Instructions for Linux
1. Clone the repository 
2. Run 'make install' as root

```bash
git clone git@gitlab.com:jeffmartin/i3-tree-parser.git
cd i3-tree-parser
sudo make install
```

*Note: The library can be un-installed with 'make uninstall'.

### Dependencies
1. jsoncpp library
