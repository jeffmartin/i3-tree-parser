/**
 * File: Display.cc
 * Author Jeffrey Martin - jeffrey.martin04@gmail.com
 */

#include <i3treeparser/Display.h>

#include <iostream>

#include <json/json.h>

#include <i3treeparser/Workspace.h>

namespace i3treeparser{

Display::Display(Json::Value json_tree_display, Json::Value json_display, 
        Json::Value json_workspaces){
    focused = json_tree_display["focused"].asBool();
    urgent = json_tree_display["urgent"].asBool();
    name = json_tree_display["name"].asString();
    active = json_display["active"].asBool();
    primary = json_display["primary"].asBool();

    // Iterate over each pane in the display
    for(int i = 0; i < json_tree_display["nodes"].size(); i++){
        Json::Value json_tree_pane = json_tree_display["nodes"][i];
        // Skip the "topdock" & "bottomdock" panes
        if(!json_tree_pane["name"].asString().compare("topdock") ||
                !json_tree_pane["name"].asString().compare("bottomdock")){
            continue;
        }
        for(int j = 0; j < json_tree_pane["nodes"].size(); j++){
            Json::Value json_tree_workspace = json_tree_pane["nodes"][j];
            std::string workspace_name = json_tree_workspace["name"].asString();
            // Find the corrusponding display object in json_displays;
            Json::Value json_workspace;
            for(int j = 0; j < json_workspaces.size(); j++){
                if(!workspace_name.compare(json_workspaces[j]["name"].asString())){
                    json_workspace = json_workspaces[j];
                    break;
                }
            }
            workspaces.push_back(new Workspace(json_tree_workspace, json_workspace));
        }
    }
}

}; // namespace i3treeparser

