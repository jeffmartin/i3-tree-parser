/**
 * File: Window.h
 * Author: Jeffrey Martin - jeffrey.martin04@gmail.com
 */

#ifndef I3TREEPARSER_WINDOW_H
#define I3TREEPARSER_WINDOW_H

#include <string>
#include <vector>

#include <json/json.h>

namespace i3treeparser{
/**
 * This class represents a single window within the i3 window manager.
 */
class Window{

private:
    /** Is the window flagged as in focus */
    bool focused; 
    /** Is the window flagged as urgent */
    bool urgent;
    /** The dimensions/coordinates of the window */
    int height; int width; int x; int y;
    /** The window type */
    std::string type;
    /** The window title */
    std::string title;

public:
    /** 
      * @param json_window The Json object representing this Window. 
      * Retrieved from the TreeBuilder's tree_query. 
      */
    Window(Json::Value json_tree_window);
    /** @return True if the window is focused, False otherwise. */
    bool isFocused(){ return focused; };
    /** @return True if the window is urgent, False otherwise. */
    bool isUrgent(){ return urgent; };
    /** @return The height of the window. */
    int getHeight(){ return height; };
    /** @return The width of the window. */
    int getWidth(){ return width; };
    /** @return The x coordinate of the top left window corner. */
    int getXCoord(){ return x; };
    /** @return The y coordinate of the top left window corner. */
    int getYCoord(){ return y; };
    /** @return The type of the window. */
    std::string getType(){ return type; };
    /** @return The title of the window */
    std::string getTitle(){ return title; };

}; // class Window

}; // namespace i3treeparser

#endif // I3TREEPARSER_WINDOW_H
