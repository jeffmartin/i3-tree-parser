/**
 * File: i3treeparser.h
 * Author: Jeffrey Martin - jeffrey.martin04@gmail.com
 *
 * This is the primary header file for the i3-tree-parser library.
 */

#ifndef I3TREEPARSER_I3TREEPARSER_H
#define I3TREEPARSER_I3TREEPARSER_H

#include <i3treeparser/TreeBuilder.h>
#include <i3treeparser/Tree.h>
#include <i3treeparser/Display.h>
#include <i3treeparser/Workspace.h>
#include <i3treeparser/Layout.h>
#include <i3treeparser/Window.h>

#endif // I3TREEPARSER_I3TREEPARSER_H
