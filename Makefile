objects = TreeBuilder.o Tree.o Display.o Workspace.o Layout.o Window.o
headers = i3treeparser.h TreeBuilder.h Tree.h Display.h Workspace.h Layout.h Window.h
library = libi3treeparser.a

LIBRARY_DIR = /usr/lib/
INCLUDE_DIR = /usr/include/i3treeparser/

install:
	mkdir -p $(LIBRARY_DIR)
	mkdir -p $(INCLUDE_DIR)
	cp $(headers) $(INCLUDE_DIR)
	make library
	cp $(library) $(LIBRARY_DIR)

library: $(objects)
	ar rs $(library) $(objects)

uninstall:
	rm -f $(LIBRARY_DIR)$(library)
	rm -r -f $(INCLUDE_DIR)

clean:
	rm -f $(objects)
	rm -f $(library)
