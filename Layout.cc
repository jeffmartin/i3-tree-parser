/**
 * File: Layout.cc
 * Author Jeffrey Martin - jeffrey.martin04@gmail.com
 */

#include <i3treeparser/Layout.h>

#include <iostream>

#include <json/json.h>

#include <i3treeparser/Window.h>

namespace i3treeparser{

Layout::Layout(Json::Value json_tree_layout){
    if (json_tree_layout["nodes"].size() == 0){ // This is a leaf layout
        type = "container";
        if(json_tree_layout["window"].asString().compare("null")){ // If is a window
            child = new Window(json_tree_layout);
        }else{
            child = NULL;
        }
    }else{ // This has sub layouts.
        type = json_tree_layout["layout"].asString();
        for(int i = 0; i < json_tree_layout["nodes"].size(); i++){
            Json::Value json_tree_sub_layout = json_tree_layout["nodes"][i];
            sub_layouts.push_back(new Layout(json_tree_sub_layout));
            child = NULL;
        }
    }
}

}; // namespace i3treeparser

